import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'My Clock',
    home: HomeScreen(),
  ));
}

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  Animation animation;
  AnimationController animationController;

  _CurrentTime() {
    return " ${DateTime.now().hour} : ${DateTime.now().minute}";
  }

  @override
  void initState() {
    super.initState();
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));

    animationController.addListener(() {
      if (animationController.isCompleted) {
        animationController.reverse();
      } else if (animationController.isDismissed) {
        animationController.forward();
      }

      setState(() {});
    });
    animationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    animation =
        CurvedAnimation(parent: animationController, curve: Curves.easeInOut);
    animation = Tween(begin: -0.5, end: 0.5).animate(animation);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        elevation: 0.0,
        title: Center(
            child: Text(
          'Clock',
          style: TextStyle(
              fontSize: 25.0, color: Colors.white, fontWeight: FontWeight.bold),
        )),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.deepOrange,
        child: Center(
          child: Column(
            children: <Widget>[
              Material(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                elevation: 12.0,
                color: Colors.brown.shade900,
                child: Container(
                  width: 280,
                  height: 280,
                  child: Center(
                    child: Text(
                      _CurrentTime(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 70.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ),
              ),
              Transform(
                alignment: FractionalOffset(0.5, 0.0),
                transform: Matrix4.rotationZ(animation.value),
                child: Container(
                    child: Image.asset(
                  'images/pendulum.png',
                  width: 100.0,
                  height: 250.0,
                )),
              )
            ],
          ),
        ),
      ),
    );
  }
}
